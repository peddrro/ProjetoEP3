class CreateProdutos < ActiveRecord::Migration
  def change
    create_table :produtos do |t|
      t.string :conteudo
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :produtos, [:user_id, :created_at]
  end
end
