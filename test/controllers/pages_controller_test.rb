require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Home | Play.Market"
  end

  test "should get sobre" do
    get :sobre
    assert_response :success
    assert_select "title", "Sobre | Play.Market"  
  end

  test "should get ajuda" do
    get :ajuda
    assert_response :success
    assert_select "title", "Ajuda | Play.Market"
  end

end
