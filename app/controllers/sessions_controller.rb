class SessionsController < ApplicationController
  def new
  end
  def create
  	usuario = User.find_by(email:params[:session][:email].downcase)
  	if usuario && usuario.authenticate(params[:session][:password])
  		login usuario
  		redirect_to usuario
  	else
  		render 'new'
  	end
  end

  def destroy
    logout if logged_in?
    redirect_to root_url
  end

end
