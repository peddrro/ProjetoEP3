module ApplicationHelper
	def titulo(pag_titulo = '')
    titulo = "Play.Market"
    if pag_titulo.empty?
      titulo
    else
      pag_titulo + " | " + titulo
    end
  end
end
