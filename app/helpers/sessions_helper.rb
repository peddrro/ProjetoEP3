module SessionsHelper
	#Loga o usuario passado
	#session = metodo do Rails que ajuda a realizar log in
	def login(user)
		session[:id_usuario] = user.id
	end
	def usuario_atual
    if (id_usuario = session[:id_usuario])
    		
   	@usuario_atual = @usuario_atual || User.find_by(id: session[:id_usuario])
   end
  	end

  	def usuario_atual?(user)
  		user == usuario_atual
  	end

  	def logged_in?
    !usuario_atual.nil?
 	end

 	def logout
 		session.delete(:id_usuario)
 		@usuario_atual = nil
 	end

end
